#include <stdio.h>



int main(void) {
  int n;
  int ten=0;
  scanf("%d",&n);
  
  //次に加算するかを記録
  int nec1=0, nec2=0;
  
  //投球回数とフレームを記録
  int i=0,flame=0;
  for(i=0;i<n;i++){
    flame++;
    if(flame==11) flame--;
    
    //一投目
    int a;
    scanf("%d",&a);
    ten+=a*(nec1+1);
    nec1=nec2;
    nec2=0;

    //ストライクの時
    if(a==10){
      //10フレーム目なら、スキップ
      if(flame==10) continue;
      nec1++;
      nec2++;
      continue;
    } else {
      i++;
      
      int b;
      if(i>=n) continue;
    
      //二投目
      scanf("%d",&b);
      ten+=b*(nec1+1);
      nec1=nec2;
      
      //10フレームならスキップ
      if(flame==10) continue;
      if(a+b==10) nec1++;
    }
  }
  printf("%d\n",ten);
  return 0;
}
