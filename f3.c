#include <stdio.h>
#include <math.h>

int main(void) {
  int x;
  scanf("%d",&x);
  int f=0;
  if(x%4==0){
    if(x%100==0){
      if(x%400==0){
        f=1;
      } else {
        f=0;
      }
    } else {
      f=1;
    }
  } else {
    f=0;
  }
  if(f) printf("YES\n");
  else printf("NO\n");
  return 0;
}
